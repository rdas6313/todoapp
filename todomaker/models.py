from django.db import models
from django.urls import reverse
from django.contrib.auth import get_user_model
# Create your models here.


class TodoItems(models.Model):
    """ Todo items """

    item = models.CharField(max_length=255)
    last_modified = models.DateTimeField(auto_now=True)
    user_id = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.item}"

    def get_absolute_url(self):
        return reverse('todoitem_detail', args=[str(self.id)])

    class Meta:
        ordering = ['item']
        permissions = (
            ("view_items", "View items of todo list"),
            ("add_item", "add items to todo list"),
            ("delete_item", "Delete item from todo list"),
            ("update_item", "Update item of todo list"),
        )
