from typing import Any, Dict
from django.db.models.query import QuerySet
from django.forms.models import BaseModelForm
from django.shortcuts import render, redirect
from django.http import HttpRequest, HttpResponse
from .models import TodoItems
from django.views import generic
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from .forms import AddItemForm
from django.urls import reverse, reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.contrib.auth.decorators import login_required
# Create your views here.


# def index(request):
#     """ view function for home page """
#     query_set = TodoItems.objects.all()
#     count = query_set.count()

#     context = {
#         'count': count,
#         'items': query_set
#     }
#     return render(request, 'index.html', context=context)


class TodoItemListView(PermissionRequiredMixin, LoginRequiredMixin, generic.ListView):
    model = TodoItems
    context_object_name = 'items'  # (default name: (model_name)_list)
    template_name = 'index.html'
    paginate_by = 5
    permission_required = 'todomaker.view_items'

    def get_queryset(self) -> QuerySet[Any]:
        queryset = TodoItems.objects.all().filter(user_id=self.request.user.id)
        return queryset

    def get_context_data(self, **kwargs: Any) -> Dict[str, Any]:
        context = super().get_context_data(**kwargs)
        context['index'] = True
        context['count'] = self.get_queryset().count()
        context['page_hit'] = self.request.session.get('visits', 0) + 1
        self.request.session['visits'] = self.request.session.get(
            'visits', 0) + 1
        return context

    # def post(self, request, *args, **kwargs):
    #     self.object_list = self.get_queryset()
    #     context = self.get_context_data()
    #     form = AddItemForm(request.POST)
    #     context['form'] = form
    #     if form.is_valid():
    #         item_data = form.cleaned_data.get('item_data', None)
    #         todoitem = TodoItems(item=item_data)
    #         todoitem.save()
    #         return redirect('todoitem_list')

    #     return self.render_to_response(context)


class TodoItemCreateView(PermissionRequiredMixin, CreateView):
    model = TodoItems
    fields = ['item']
    template_name = 'index.html'
    success_url = reverse_lazy('todoitem_list')
    permission_required = 'todomaker.add_item'

    def form_valid(self, form: BaseModelForm):
        form.instance.user_id = self.request.user
        return super().form_valid(form)


class TodoItemDeleteView(PermissionRequiredMixin, DeleteView):
    model = TodoItems
    success_url = reverse_lazy('todoitem_list')
    permission_required = 'todomaker.delete_item'


class TodoItemUpdateView(PermissionRequiredMixin, UpdateView):
    model = TodoItems
    fields = ['item']
    template_name = 'item_edit.html'
    success_url = reverse_lazy('todoitem_list')
    permission_required = 'todomaker.update_item'


class TodoItemDetailView(LoginRequiredMixin, generic.DetailView):
    model = TodoItems
    template_name = 'item_detail.html'


@login_required
def about_us(request):
    return render(request, 'about_us.html')
