from django.contrib import admin

from . import models
# Register your models here.


@admin.register(models.TodoItems)
class TodoItemsAdmin(admin.ModelAdmin):
    list_display = ['item', 'last_modified']
    list_per_page = 5
    search_fields = ['item__istartswith']
