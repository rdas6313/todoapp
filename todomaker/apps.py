from django.apps import AppConfig


class TodomakerConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'todomaker'
