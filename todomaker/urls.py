from django.urls import path
from . import views

urlpatterns = [
    # path('', views.index, name='index'),
    path('', views.TodoItemListView.as_view(), name='todoitem_list'),
    path('<int:pk>', views.TodoItemDetailView.as_view(), name='todoitem_detail'),
    path('create/', views.TodoItemCreateView.as_view(), name='todoitem_create'),
    path('delete/<int:pk>', views.TodoItemDeleteView.as_view(),
         name='todoitem_delete'),
    path('update/<int:pk>', views.TodoItemUpdateView.as_view(),
         name='todoitem_update'),
    path('about-us', views.about_us, name='about_us')
]
