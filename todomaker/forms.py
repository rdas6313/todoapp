from django import forms


class AddItemForm(forms.Form):
    item_data = forms.CharField(
        error_messages={'required': 'This field is required'}, help_text='Enter your item')
