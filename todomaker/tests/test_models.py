from django.test import TestCase
from todomaker.models import TodoItems
from django.contrib.auth.models import User

# Create your tests here.
class TodoItemsModelTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        testuser = User.objects.create(username="testuser1",password='1X<Isuwestw2d')
        TodoItems.objects.create(item='Agni',user_id=testuser)

    def setUp(self):
        pass

    def test_item_label(self):
        item = TodoItems.objects.get(pk=1)
        item_label = item._meta.get_field('item').verbose_name
        self.assertEqual(item_label,'item')
    
    def test_item_max_length(self):
        item = TodoItems.objects.get(pk=1)
        max_length = item._meta.get_field('item').max_length
        self.assertEqual(max_length,255,'Max length are not equal')
    
    def test_last_modified_label(self):
        item = TodoItems.objects.get(pk=1)
        last_modified_label = item._meta.get_field('last_modified').verbose_name
        self.assertEqual(last_modified_label,'last modified')
        
    
    def test_object_name(self):
        item = TodoItems.objects.get(pk=1)
        object_name = f"{item.item}"
        self.assertEqual(object_name,str(item))
    
    def test_get_absolute_url(self):
        item = TodoItems.objects.get(pk=1)
        self.assertEqual(item.get_absolute_url(),"/todo/1")
    
    def test_permissions(self):
        item = TodoItems.objects.get(pk=1)
        permissions = (
            ("view_items", "View items of todo list"),
            ("add_item", "add items to todo list"),
            ("delete_item", "Delete item from todo list"),
            ("update_item", "Update item of todo list"),
        )
        result = all(x == y for x,y in zip(permissions,item._meta.permissions))
        self.assertTrue(result)
    