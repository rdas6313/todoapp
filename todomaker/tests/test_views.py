from django.test import TestCase
from django.contrib.auth.models import User,Permission
from todomaker.models import TodoItems
from django.urls import reverse

class TodoItemListViewTest(TestCase):

    @classmethod
    def setUpTestData(cls):

        view_permission = Permission.objects.get(codename='view_items')
        add_permission = Permission.objects.get(codename='add_item')
        update_permission = Permission.objects.get(codename='update_item')
        delete_permission = Permission.objects.get(codename='delete_item')

        testuser1 = User.objects.create_user(username="testuser1",password='123456')
        testuser1.user_permissions.add(view_permission)
        testuser1.user_permissions.add(add_permission)
        testuser1.save()
        testuser2 = User.objects.create_user(username="testuser2",password='123456')
        testuser2.save()
        for i in range(20):
            if i < 10:
                item = TodoItems.objects.create(item=f"item{i}",user_id=testuser1)
            else:
                item = TodoItems.objects.create(item=f"item{i}",user_id=testuser2)
            item.save()

    def test_redirect_if_not_logged_in(self):
        response = self.client.get("/todo/")
        self.assertRedirects(response,'/accounts/login/?next=/todo/')
    
    
    def test_forbidden_if_logged_in_but_have_no_permission(self):
        login = self.client.login(username="testuser2",password="123456")
        response = self.client.get(reverse('todoitem_list'),follow=True,secure=True)
        self.assertEqual(response.status_code,403)
    
    def test_logged_in_with_permission_uses_correct_template(self):
        login = self.client.login(username="testuser1",password="123456")
        response = self.client.get(reverse('todoitem_list'),follow=True,secure=True)
        self.assertEqual(response.status_code,200)
        self.assertTemplateUsed(response,'index.html')
    
    def test_pagination_first_page_is_having_five_item(self):
        login = self.client.login(username="testuser1",password="123456")
        response = self.client.get(reverse('todoitem_list'),follow=True)
        self.assertEqual(response.status_code,200)
        self.assertTrue('is_paginated' in response.context)
        self.assertTrue(response.context['is_paginated'] == True)
        self.assertEqual(len(response.context['items']),5)
    
    def test_pagination_second_page_is_having_five_item(self):
        login = self.client.login(username="testuser1",password="123456")
        response = self.client.get(reverse('todoitem_list')+'?page=2',follow=True)
        self.assertEqual(response.status_code,200)
        self.assertTrue('is_paginated' in response.context)
        self.assertTrue(response.context['is_paginated'] == True)
        self.assertEqual(len(response.context['items']),5)
    
    def test_item_details_if_logged_in_have_permission(self):
        login = self.client.login(username="testuser1",password="123456")
        response = self.client.get(reverse('todoitem_detail',args=[2]))
        self.assertEqual(response.status_code,200)
    
    def test_item_details_if_logged_in_have_permission_uses_correct_template(self):
        login = self.client.login(username="testuser1",password="123456")
        response = self.client.get(reverse('todoitem_detail',args=[2]))
        self.assertEqual(response.status_code,200)
        self.assertTemplateUsed(response,'item_detail.html')
    
    def test_http404_error_if_item_detail_not_found(self):
        login = self.client.login(username="testuser1",password="123456")
        response = self.client.get(reverse('todoitem_detail',args=[200]))
        self.assertEqual(response.status_code,404)
    
    def test_create_new_item_with_permission_and_logged_in(self):
        login = self.client.login(username="testuser1",password="123456")
        response = self.client.post(reverse('todoitem_create'),data={'item':'Pikachu'},follow=True)
        self.assertEqual(response.status_code,200)
        self.assertEqual(response.context['count'],11)
        data = [item.item for item in response.context['items']]
        self.assertTrue('Pikachu' in data)

    def test_create_new_item_with_out_permission_and_logged_in(self):
        login = self.client.login(username="testuser2",password="123456")
        response = self.client.post(reverse('todoitem_create'),data={'item':'Pikachu'},follow=True)
        self.assertEqual(response.status_code,403)
    
    def test_create_new_item_uses_correct_success_url(self):
        login = self.client.login(username="testuser1",password="123456")
        response = self.client.post(reverse('todoitem_create'),data={'item':'Pikachu'})
        self.assertRedirects(response,reverse('todoitem_list'))

    def test_create_new_item_with_no_item(self):
        login = self.client.login(username="testuser1",password="123456")
        response = self.client.post(reverse('todoitem_create'),data={'item':''})
        self.assertEqual(response.status_code,200)
        self.assertFormError(response,'form','item',['This field is required.'])

    


    